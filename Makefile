# pam_dbus_session helps you launch a dbus session bus
# Copyright 2020 Shironeko
#
# This file is part of pam_dbus_session.
#
# pam_dbus_session is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pam_dbus_session is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with pam_dbus_session.  If not, see <https://www.gnu.org/licenses/>.

CC = cc
CFLAGS = -g -Os -pedantic -Wall

pam_dbus_session.so : pam_dbus_session.o
	${CC} -shared -o $@ $^ -lpam

pam_dbus_session.o : pam_dbus_session.c
	${CC} ${CFLAGS} -fPIC -c $^

.PHONY : clean install

clean :
	rm pam_dbus_session.o

install : pam_dbus_session.so
	strip --strip-all pam_dbus_session.so
	cp pam_dbus_session.so /usr/lib/security/
