/*
 * pam_dbus_session helps you launch a dbus session bus
 * Copyright 2020 Shironeko
 *
 * This file is part of pam_dbus_session.
 *
 * pam_dbus_session is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pam_dbus_session is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with pam_dbus_session.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE

#include <errno.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <security/pam_modules.h>
#include <security/pam_modutil.h>

static int
get_uid_gid (pam_handle_t *pamh, uid_t *uid, gid_t *gid)
{
  int ret;

  const char *user;
  struct passwd *pw;

  if ((ret = pam_get_user (pamh, &user, NULL)) != PAM_SUCCESS)
    return ret;
  if (!(pw = getpwnam (user)))
    return PAM_USER_UNKNOWN;
  *uid = pw->pw_uid;
  *gid = pw->pw_gid;
  return PAM_SUCCESS;
}

static int
get_env (pam_handle_t *pamh, int argc, const char **argv, char **addr)
{
  const char *xdg_dir;

  if (argc)
    {
      if ((*addr = strdup (argv[0])) == NULL)
        return PAM_SYSTEM_ERR;
    }
  else
    {
      if ((xdg_dir = pam_getenv (pamh, "XDG_RUNTIME_DIR")) == NULL)
        return PAM_SYSTEM_ERR;
      if (asprintf (addr, "unix:path=%s/bus", xdg_dir) < 0)
        return PAM_SYSTEM_ERR;
    }
  return PAM_SUCCESS;
}

static int
set_env (pam_handle_t *pamh, char **addr)
{
  int ret;

  char *name_value;

  if (asprintf (&name_value, "DBUS_SESSION_BUS_ADDRESS=%s", *addr) < 0)
    return PAM_SYSTEM_ERR;
  if ((ret = pam_putenv (pamh, name_value)) != PAM_SUCCESS)
    return ret;
  free (name_value);
  return PAM_SUCCESS;
}

static int
spawn_daemon (pam_handle_t *pamh, uid_t uid, gid_t gid, char *addr)
{
  int ret;
  pid_t pid;
  char *cargv[] = {"/usr/bin/dbus-daemon", "--session", "--fork", NULL, NULL};
  if ((pid = fork ()) == -1)
    return PAM_SYSTEM_ERR;
  else if (pid == 0)
    {
      ret = asprintf (&cargv[2], "%s.*--address=%s.*", cargv[0], addr);
      free (addr);
      if (ret < 0)
        {
          return PAM_SYSTEM_ERR;
        }
      cargv[0] = "/usr/bin/pkill";
      cargv[1] = "-f";
      execv (cargv[0], cargv);
      free (cargv[2]);
    }
  else
    {
      int status;
      while (waitpid (pid, &status, 0) == -1 && errno == EINTR)
        ;
      ret = asprintf (&cargv[3], "--address=%s", addr);
      free (addr);
      if (ret < 0)
        return PAM_SYSTEM_ERR;
      char **envlist = pam_getenvlist (pamh);
      if (envlist == NULL)
        {
          free (cargv[3]);
          return PAM_SYSTEM_ERR;
        }
      ret = pam_modutil_sanitize_helper_fds (pamh, PAM_MODUTIL_PIPE_FD,
                                             PAM_MODUTIL_NULL_FD,
                                             PAM_MODUTIL_NULL_FD);
      if (!(ret < 0 || setgid (gid) || setuid (uid)))
        execve (cargv[0], cargv, envlist);
      free (cargv[3]);
      free (envlist);
    }
  return PAM_SYSTEM_ERR;
}

int
pam_sm_open_session (pam_handle_t *pamh, int flags, int argc, const char **argv)
{
  int ret;

  uid_t uid;
  gid_t gid;
  pid_t pid;

  char *addr;

  if ((ret = get_uid_gid (pamh, &uid, &gid)) != PAM_SUCCESS || uid == 0)
    return ret;
  if ((ret = get_env (pamh, argc, argv, &addr)) != PAM_SUCCESS)
    return ret;
  if (!(strncmp (addr, "unix:path=", strlen ("unix:path="))
        || (access (addr + strlen ("unix:path="), F_OK))))
    {
      free (addr);
      return PAM_SUCCESS;
    }

  if ((pid = fork ()) == -1)
    return PAM_SYSTEM_ERR;
  else if (pid == 0)
    {
      spawn_daemon (pamh, uid, gid, addr);
      _exit (errno);
    }
  else
    {
      int status;

      while (waitpid (pid, &status, 0) == -1 && errno == EINTR)
        ;
      ret = set_env (pamh, &addr);
      free (addr);
      return ret;
    }
}

int
pam_sm_close_session (pam_handle_t *pamh, int flags, int argc,
                      const char **argv)
{
  return PAM_SUCCESS;
}
